
const
    monsters: array [0..MONSTERS_COUNT - 1] of TString = (
        'NIETOPERZ', // 0
        'SZCZUR',
        'JASZCZUR',
        'PAJ'#2'K',
        'PIES',
        'W'#2#21,
        'KRONK',
        'SKRZAT',
        'GOBLIN',
        'ZOOMBIE',
        'WARG',     // 10
        'DUCH',
        'ELF',
        'WILKO'#12'AK',
        'CZAROWNICA',
        'ORK',
        'GARGOYLA',
        'TROLL',
        'CZAROWNIK',
        'WAMPIR',
        'UPI'#15'R', // 20
        'MINOTAUR',
        'CYKLOP',
        'GIGANT',
        'HYDRA',
        'DEMON',
        'MEDUZA',
        'SMOK',
        'DIABE'#12,
        'FENIKS'
     );
    weapons: array [0..WEAPONS_COUNT - 1] of TString = (
        'WIDELEC','SCYZORYK','FINK'#11,'SZTYLET','SZPAD'#11,'LANC'#11,'OSZCZEP','SZABL'#11,'TOP'#15'R','MIECZ' );
    treasures: array [0..TREASURES_COUNT - 1] of TString = (
        'BR'#2'Z','SREBRO','Z'#12'OTO','PLATYN'#11,'DIAMENTY' );
    items: array [0..ITEMS_COUNT - 1] of TString = (        
        'M'#12'OTEK','KAGANEK','KLUCZ','DESK'#11,'PROWIANT','NAPOJE','BANDA'#21,#12'UBKI','HAS'#12'O' );
    itemSymbols: array [0..ITEMS_COUNT - 2] of char = ( 'M', 'G', 'K', 'D', 'P', 'N', 'B', 'L');
    passwords: array [0..PASSWORDS_COUNT - 1] of TString = (
        'RYBA','JAJO','BAJT','TRZY','KLAN','KREW','MYSZ','KLOC','BICZ');
    texts: array [0..3] of TString = ('WI'#11'C WYRZUCI'#12'E'#9'.','U'#21'Y'#12'E'#9' ','NIE MASZ ','ZAATAKOWA'#12);


function needPostfix(monster: byte): boolean;
begin
    result := (monster = 14) or (monster = 16) or (monster = 26) or (monster = 24);
end;


procedure TitleScreen;
begin
    InitGraph(8);
    Pause;
    nmien := $40;
    color2 := 0;
    color1 := 0;
    Move(pointer(TITLE_BASE),pointer(savmsc + (40 * 8)),5840);
    for i:=0 to 12 do begin
        color1 := i;
        Pause(2);
    end;
    readkey;
    
    for i:=12 to 0 do begin
        color1 := i;
        Pause;
    end;

end;
