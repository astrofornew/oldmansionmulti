program starydom;
uses atari, crt, graph, efast;

type
  String40 = string[40];
  player = record
    inventory:TString;
    weaponName:TString;
    x:byte;
    y:byte;  // player position
    xstart:byte;
    ystart:byte;
    currentLocation: byte;
    weapon: shortInt;
    wounds: shortInt;
    gold: smallInt;
    score: cardinal;
    strength: real;
    energy: real;
  END;


const
{$i const.inc}
{$r resources.rc}

var
    currentinventory:TString;
    currentplayer,opponentplayer:^player;
    p1,p2,p3,p4:player;
    players : array[0..3] of ^player;
    fscr: file;
    inventory, currentPassword,weaponName: TString;
    aStr, bStr: TString; // reusable temporary strings
    x, y: byte; // player position
    i: byte; // common iterator
    currentLocation, keycode: byte;
    q, r: byte;  // randoms
    weapon,wounds: shortInt;
    gold: smallInt;
    score: cardinal;
    strength,energy, monsterStrength, monsterSize: real;
    gameEnded: boolean;
    numberofplayers:shortInt;
    currentplayerindex:shortInt;
    allitems:STRING[15];
    goldenkeyowner:shortInt;


procedure dli:assembler;interrupt;
asm {
    pha
@   sta wsync
    lda vcount
    cmp #82
    bmi @-
    lda #0          ; stats top
    sta colpf2
    lda #12
    sta colpf1
@   sta wsync
    lda vcount
    cmp #102
    bmi @-
    lda #$fe        ; dialog top
    sta wsync
    sta colbak
    sta colpf2
    lda #2
    sta colpf1
@   sta wsync
    lda vcount
    cmp #113
    bmi @-
    lda #0          ; dialog bottom
    sta wsync
    sta colbak
    sta colpf2
    pla
};
end;

// ************************************* helpers

function Antic2Atascii(c: byte):byte;assembler;overload;
asm {
    lda c
    asl
    php
    cmp #2*$60
    bcs @+
    sbc #2*$40-1
    bcs @+
    adc #2*$60
@   plp
    ror
    sta result;
    };
end;

function strCmp(a, b: TString): boolean;
var i:byte;
begin
    result:= true;
    for i:=0 to length(a)-1 do
        if a[i]<>b[i] then exit(false);
end;

function formatFloat(num: real):TString;
var m: cardinal;
    ms: TString;
begin
    Str(Trunc(num), result);
    m := Trunc(Frac(num) * 1000.0);
    if m > 0 then begin
        Str(m, ms);
        while (length(ms) < 3) do ms := concat('0', ms);
        result := concat(result, '.');
        result := concat(result, ms);
        while (result[byte(result[0])]='0') do dec(result[0]);
    end;
end;

// ************************************* I/O routines

procedure print(s: String40);overload;
begin
    blockwrite(fscr, s[1], length(s));
end;

procedure print(c: char);overload;
begin
    blockwrite(fscr, c, 1);
end;

procedure print(c1, c2: char);overload;
begin
    blockwrite(fscr, c1, 1);
    blockwrite(fscr, c2, 1);
end;

procedure position(x, y: byte);
begin
    gotoxy(x + 1, y + 1);
end;

function locate(x, y: byte):byte;
begin
    result := Antic2Atascii(peek(savmsc + (y * 40) + x));
end;

function getKey:byte;overload;
begin
    result := byte(readkey) and %01011111;
end;

function getKey(a, b:byte):byte;overload;
begin
    repeat
        result := byte(readkey) and %01011111;
    until (result = a) or (result = b);
end;

function getKey(a, b, c, d: byte):byte;overload;
begin
    repeat
        result := byte(readkey) and %01011111;
    until (result = a) or (result = b) or (result = c) or (result = d);
end;




{$i lang_pl.inc}
{ $i lang_en.inc}


procedure PromptAny;
begin
   position(8, 23);
   write('NACI'#9'NIJ DOWOLNY KLAWISZ');
   ReadKey;
   write(#125);
end;



procedure ClearLine;
begin
    position(2, 22); write(#156,#156);
end;

procedure statusLine(s: String40);
begin
    ClearLine;
    position(2, 22);
    write(s);
end;

procedure statusLineln(s: String40);
begin
    statusLine(s);
    writeln;
end;

procedure statusLine2(s: String40);
begin
    position(2, 23);
    write(s);
end;

// ************************************* initializers

procedure switchCurrentPlayerVar;
begin
  players[0] := @p1;
  players[1] := @p2;
  players[2] := @p3;
  players[3] := @p4;
  currentplayer := players[currentplayerindex];
  //currentinventory :=  currentplayer^.inventory;
end;

procedure initPlayersStartPositions;
BEGIN
  if currentplayerindex=0 then BEGIN
    currentplayer^.xstart := 6;
    currentplayer^.ystart := 1;
  END;

  if currentplayerindex=1 then BEGIN
    currentplayer^.xstart := 6;
    currentplayer^.ystart := 15;
  END;

  if currentplayerindex=2 then BEGIN
      currentplayer^.xstart := 34;
      currentplayer^.ystart := 1;
  END;
  currentplayer^.x := currentplayer^.xstart;
  currentplayer^.y := currentplayer^.ystart;
END;

procedure VarInitSinglePlayer;
begin
    initPlayersStartPositions;
    //fillchar(currentplayer^.inventory, 4, TILE_EMPTY_SLOT);
    currentplayer^.inventory := '----';
    //fillchar(p1.inventory[1], 4, TILE_EMPTY_SLOT);
    //p1.inventory[0] := char(4);

    fillchar(allitems[1], 15, TILE_EMPTY_SLOT);
    allitems[0] := char(15);

    currentplayer^.weapon := 1;
    currentplayer^.gold := 0;
    currentplayer^.wounds := 0;
    currentplayer^.energy := 3 ;
    currentplayer^.currentLocation := TILE_ROOM;
    currentplayer^.weaponName := weapons[currentplayer^.weapon - 1];
    //set golden key owner out of player index  range
    goldenkeyowner:=6;
    //if(word(@currentplayer)=word(@currentplayer))then  write('');


    fillchar(inventory[1], 4, TILE_EMPTY_SLOT);
    inventory[0] := char(4);
    currentPassword := passwords[Random(9)];
    weapon := 1;
    gold := 0;
    wounds := 0;
    energy := 3 ;

    weaponName := weapons[weapon - 1];
end;


procedure VarInit;
begin
  for i := 1 to numberofplayers do begin
  switchCurrentPlayerVar;
  VarInitSinglePlayer;
  currentplayerindex:= currentplayerindex+1;
  end;
end;

// ************************************* GUI

procedure ShowManual;
begin
    InitGraph(0);
    pause;
    nmien := $40;
    color2 := 0;
    chbas := Hi(CHARSET_BASE);
    Writeln;
    Write('   CZY CHCESZ INSTRUKCJE ? (T/N) ');
    keycode := getKey;
    if keycode <> 84 then exit;
    CursorOff;
    Writeln;
    Writeln;
    Writeln(' Znalaz'#123'e'#10' si'#20' nagle w starym domu,   z kt'#16'rego musisz si'#20' wydosta'#22'.');
    Writeln('Jest tu tylko jedno wyj'#10'cie i jest    ono strze'#21'one. Stra'#21'nik wypu'#10'ci ci'#20',');
    Writeln('je'#10'li podasz has'#123'o i zap'#123'acisz 100 $.');
    Writeln;
    Writeln(' Musisz zbiera',#22,' pieni',#8,'dze (w formie');
    Writeln('skarb'#16'w), kt'#16're s'#8' w'#123'asno'#10'ci'#8' potwo-  r'#16'w zamieszkuj'#8'cych dom. Mo'#21'esz z');
    Writeln('nimi walczy'#22', je'#10'li masz mniej ni'#21'    5 ran i si'#123''#20' wi'#20'ksz'#8' od zera.');
    Writeln;
    Writeln(' Mo'#21'esz zwi'#20'kszy'#22' swoj'#8' energi'#20', gdy  znajdziesz lub kupisz prowiant lub');
    Writeln('napoje. Rany mo'#21'esz leczy'#22' '#123'ubkami    lub banda'#21'ami.');
    PromptAny;

    Writeln;
    Writeln(' Je'#21'eli nie mo'#21'esz walczy'#22', to musisz zap'#123'aci'#22' potworowi za wypuszczenie.');
    Writeln;
    Writeln(' Has'#123'o znajdziesz w domu. Musisz je   dobrze zapami'#20'ta'#22'.');
    Writeln;
    Writeln;
    Writeln(' Dom jest bardzo stary. W niekt'#16'rych  pokojach jest ciemno, w innych nie');
    Writeln('ma pod'#123'ogi. Mo',#21,'esz tak'#21'e napotka'#22'');
    Writeln('zamkni'#20'te drzwi lub cienkie '#10'cianki.');
    Writeln('Dlatego zbieraj napotkane przedmioty, ale pami'#20'taj, '#21'e mo'#21'esz jednocze'#10'nie');
    Writeln('nie'#10#22' tylko cztery przedmioty i jed-  n'#8' bro'#13'. Wybieraj zawsze najlepsz'#8'');
    Writeln('bro'#13'; zale'#21'y od tego twoja si'#123'a oraz  wielko'#10#22' zdobywanych skarb'#16'w.');
    PromptAny;

    Writeln;
    Writeln('           SKR'#15'TY:');
    Writeln;
    print(char(TILE_EXIT),char(TILE_EXIT2)); writeln(' - wyj'#10'cie');
    print(char(TILE_DARK)); writeln(' - ciemno');
    print(char(TILE_HOLE)); writeln(' - brak pod'#123'ogi');
    print(char(TILE_DOOR_H), char(TILE_DOOR_V)); writeln( ' - zamkni'#20'te drzwi');
    print(char(TILE_WALL_H), char(TILE_WALL_V)); writeln( ' - cienka '#10'cianka');
    Writeln('M - m'#123'otek');
    Writeln('G - kaganek');
    Writeln('K - klucz');
    Writeln('D - deska');
    Writeln('P - prowiant');
    Writeln('N - napoje');
    Writeln('B - banda'#21'e');
    Writeln('L - '#123'ubki');
    Writeln(TILE_PLAYER,' - a to jeste'#10' TY');
    PromptAny;
end;

function randomEntranceV:byte;
var r: byte;
begin
    result := TILE_ENTRANCE_V;
    r := random(5);
    if r = 0 then result := TILE_DOOR_V;
    if r = 1 then result := TILE_WALL_V;
end;

function randomEntranceH:byte;
var r: byte;
begin
    result := TILE_ENTRANCE_H;
    r := random(5);
    if r = 0 then result := TILE_DOOR_H;
    if r = 1 then result := TILE_WALL_H;
end;

procedure setPlayersStartPositions;
begin
    for i := 0 to numberofplayers-1 do begin
      position(players[i].x, players[i].y);
      print(TILE_PLAYER);
    end;
end;




procedure PaintBoard;
var row, room:byte;
begin
    initGraph(0);
    CursorOff;
    color2 := 0;
    color1 := 0;
    savmsc := SCREEN_BASE;

    VarInit;
    writeln(#125);
    writeln;
    fillbyte(pointer(SCREEN_BASE),40*17,128);
    fillbyte(pointer(SCREEN_BASE+40*17),5,$ff);
    fillbyte(pointer(SCREEN_BASE+40*17+36),4,$ff);

    position(5, 0); // top row of board
    print(#17);
    for room:=1 to 14 do
        print(char(TILE_BORDER_H),#23);
    print(char(TILE_BORDER_H), #5);

    for row:=0 to 7 do begin

        // rooms
        position(5, row * 2 + 1);
        print(char(TILE_BORDER_V));
        for room:=1 to 14 do
            print(char(TILE_ROOM),char(randomEntranceV));
        print(char(TILE_ROOM), char(TILE_BORDER_V));

        // inner walls
        position(5, row * 2 + 2);
        print(#1);
        for room:=1 to 14 do
            print(char(randomEntranceH), #19);
        print(char(randomEntranceH), #4);

    end;


    position(5, 16); // bottom row of board
    print(#26);
    for room:=1 to 14 do print(char(TILE_BORDER_H), #24);
    print(char(TILE_BORDER_H), #3);

    position(7, 1); print(char(TILE_ENTRANCE_V));
    position(6, 2); print(char(TILE_ENTRANCE_H));
    setPlayersStartPositions;


    position(34, 15); print(char(TILE_EXIT),char(TILE_EXIT2));

    Pause;
    chbas := Hi(CHARSET_BASE);
    SDLSTL := DISPLAY_LIST_BASE;
    nmien := $c0; // set $80 for dli only (without vbl)
    color2 := $10;
    color1 := $10;
    for i:=$10 to $1a do begin
            pause(2);
            color2 := i;
    end;

end;


procedure ShowCurrentUser;
BEGIN
   //write('ENERGIA', ' = ', formatFloat(currentplayer.energy));
   //write('RANY',currentplayer.wounds);
   //write('SKARBY', currentplayer.gold);
  //write('OBIEKTY',currentplayer.inventory);

  position(2, 9);
  //writeln('inv',currentplayer^.inventory,' ',players[0].inventory,' ',players[1].inventory);
  writeln('pos',currentplayer^.x,':',currentplayer^.y,'  p1',players[0].x,':',players[0].y,' p2',players[1].x,':',players[1].y,' p3',players[2].x,':',players[2].y);
  write('allitems',allitems);
END;

function getAllItemsForPlayer(p: SMALLINT):STRING;overload;
BEGIN
  exit(copy(allitems,p*4+1, 4));
END;

function getAllItemsForPlayer:STRING;overload;
BEGIN
  exit(copy(allitems,currentplayerindex*4+1, 4));
END;

procedure ShowStats;
var z: real;
begin
    //ShowCurrentUser;
    if currentplayer.energy < 0 then currentplayer^.energy := 0;
    position(2, 18); write('ENERGIA'*, ' = ', formatFloat(currentplayer^.energy), '    ');
    position(22, 18); write('RANY'*, ' = ', currentplayer^.wounds, '    ');
    z := currentplayer^.energy - currentplayer^.wounds;
    if z < 0 then z := 0.1;
    position(2, 19); write('SKARBY'*,' = ', currentplayer^.gold, '$   ');
    currentinventory := getAllItemsForPlayer;
    position(22, 19); write('OBIEKTY'*,' = ',currentinventory);
    position(2, 20); write('JAK'*#130' BRO'*#142' MASZ'*, ' ',    currentplayer^.weaponName,'     ');
    currentplayer^.strength := z * (1 + currentplayer^.weapon * 0.25);
    i:=0;
    if(goldenkeyowner=currentplayerindex) then i:=1;
    position(2, 21); write('SI'*#140'A'*, ' = ', formatFloat(currentplayer^.strength),'         ', 'GRACZ:'*,currentplayerindex+1,' ','GK:'*,i);
end;



function replaceAllItemsForPlayer(str:STRING;p:SMALLINT):STRING;overload;
BEGIN
  allitems[p*4+1]:=str[1];
  allitems[p*4+2]:=str[2];
  allitems[p*4+3]:=str[3];
  allitems[p*4+4]:=str[4];
END;

function replaceAllItemsForPlayer(str:STRING):STRING;overload;
BEGIN
  allitems[currentplayerindex*4+1]:=str[1];
  allitems[currentplayerindex*4+2]:=str[2];
  allitems[currentplayerindex*4+3]:=str[3];
  allitems[currentplayerindex*4+4]:=str[4];
END;




// ************************************* inventory operations


function hasItem(c: char):boolean;
var i: byte;
begin
    //currentinventory :=  currentplayer^.inventory;
    currentinventory := getAllItemsForPlayer;
    //Writeln('curinv'*, currentinventory);
    //getKey;

    result := false;
    for i := 1 to 4 do
        if currentinventory[i] = c then exit(true);
end;

procedure delItem(c: char);
var i: byte;
begin
    //currentinventory :=  currentplayer^.inventory;
    currentinventory := getAllItemsForPlayer;
    for i := 1 to 4 do
        if currentinventory[i] = c then begin
            currentinventory[i] := TILE_EMPTY_SLOT;
            replaceAllItemsForPlayer(currentinventory);
            exit;
        end;
end;

procedure addItem(c: char);
var i: byte;
begin
   //currentinventory :=  currentplayer^.inventory;

    for i := 1 to 4 do
        if currentinventory[i] = TILE_EMPTY_SLOT then begin
            currentinventory[i] := c;
            currentplayer^.inventory := currentinventory;
            replaceAllItemsForPlayer(currentinventory);
            exit;
        end;
end;


// ********************************************** main turn logic

procedure MakeMove;

var door, room: byte;
    dx, dy: shortInt;
    isIn, stepFinished, waited, skipMonster: boolean;
    itemLost: char;


procedure payRansom;
begin
    currentplayer^.gold := round( currentplayer^.gold - round(monsterSize * random));  // pay ransom
    if currentplayer^.gold < 0 then currentplayer^.gold := 0;
    stepFinished := true;
    ShowStats;
end;

procedure foundWeapon;
begin
    r := random(10) + 1;
    statusLine('ZNALAZ'#12'E'#9' ');
    writeln(weapons[r - 1]);
    write('B'*'IERZESZ CZY ', 'Z'*'OSTAWIASZ ?');
    keycode := getKey(66,90);
    if keycode = 66 then begin
        currentplayer^.weaponName := weapons[r - 1];
        currentplayer^.weapon := r;
        ShowStats;
    end;
end;

procedure foundPassword;
begin
    statusLine('ZNALAZ'#12'E'#9' HAS'#12'O, BRZMI ONO ');
    writeln(currentPassword, '.');
    write('ZAPAMI'#11'TAJ JE.');
    pause(200);
end;

procedure foundGoldenKey;
begin
    statusLine('ZNALAZ'#12'E'#9' Z'#12'OTY KLUCZ');
    writeln('NIEZBEDNY DO OPUSZCZENIA DOMU');
    goldenkeyowner:=currentplayerindex;
    pause(200);
end;



procedure foundItem;
begin
    statusLine('ZNALAZ'#12'E'#9' ');
    writeln(items[r - 1]);
    write('B'*,'IERZESZ CZY ','Z'*,'OSTAWIASZ ?');
    keycode := getKey(66,90);
    if keycode = 90 then begin
        foundWeapon;
    end else begin
        if hasItem(TILE_EMPTY_SLOT) then begin
            addItem(itemSymbols[r - 1]);
            ShowStats;
        end else begin
            stepFinished := false;
            repeat
                statusLine2('CO CHCESZ ZOSTAWI'#96' ?       ');
                repeat
                    keycode := getKey;
                until (keycode > 65) and (keycode < 90);
                if hasItem(char(keycode)) then begin
                    delItem(char(keycode));
                    addItem(itemSymbols[r - 1]);
                    ShowStats;
                    stepFinished:=true;
                end else begin
                    statusLine2('NIE MASZ '#39);
                    write(char(keycode), #39'         ');
                    readkey;
                end;
            until stepFinished;
        end;
    end;
end;


function getPlayerIndexFromPosition(px:SHORTINT;py:SHORTINT):SHORTINT;
BEGIN
    for i := 1 to numberofplayers do begin
      if ((players[i].x=px) and (players[i].y=py)) then exit(i);
    END;

END;


begin
    ShowStats;
    isIn := false;
    stepFinished := false;
    skipMonster := false;
    waited := false;

    ClearLine;
    position(10, 22); write('C'*,'ZEKANIE CZY ','R'*,'UCH ?');
    keycode := getKey(82, 67);

    if keycode = 67 then begin      // ************* waiting
        if currentplayer^.gold < 5 then begin
            currentplayer^.energy := currentplayer^.energy + 0.5;
        end else begin
            currentplayer^.gold := currentplayer^.gold - 5;
            currentplayer^.energy := currentplayer^.energy + 2;
        end;
        waited := true;
        stepFinished := true;
        ShowStats;

    end else begin                  // ************* moving
        position(7, 22);
        write('L'*,'EWO, ','P'*,'RAWO, ','G'*,#15'RA, ','D'*,#15#12' ?');
        keycode := getKey(76, 80, 71, 68);
        ClearLine;
        dx := 0;
        dy := 0;
        case keycode of
            76: dx := -1;
            80: dx := 1;
            68: dy := 1;
            71: dy := -1;
        end;

        door := locate(currentplayer^.x + dx, currentplayer^.y + dy);
        //position(1,1); write(door);

        if (door <> TILE_BORDER_H) and (door <> TILE_BORDER_H) then begin // not a border ?

            if (door = TILE_ENTRANCE_H) or (door = TILE_ENTRANCE_V) then begin     // ***********************  check doors
                statusLine('OTWARTE DRZWI, PRZECHODZISZ.');
                isIn := true;
            end else begin
                if (door = TILE_DOOR_H) or (door = TILE_DOOR_V) then begin
                    statusLine('ZAMKNI'#11'TE DRZWI, ');
                    if hasItem(itemSymbols[2]) then begin
                        write(texts[1], 'KLUCZ.');
                        isIn := true;
                    end else begin
                        write(texts[2], 'KLUCZA.');
                    end;
                end;
                if (door = TILE_WALL_H) or (door = TILE_WALL_V) then begin
                    statusLine('CIENKA '#9'CIANKA, ');
                    if hasItem(itemSymbols[0]) then begin
                        write(texts[1], 'M'#12'OTEK.');
                        isIn := true;
                    end else begin
                        write(texts[2], 'M'#12'OTKA.');
                    end;
                end;
                if isIn then begin
                    currentplayer^.energy := currentplayer^.energy - 0.5;
                    position(currentplayer^.x + dx, currentplayer^.y + dy);
                    if dy=0 then door := TILE_ENTRANCE_V
                    else door := TILE_ENTRANCE_H;
                    print(char(door));
                end;
            end;

            readkey;

            if isIn then begin  // *******************************   check room
                room := locate(currentplayer^.x + 2 * dx, currentplayer^.y + 2 * dy);


                if ((room = 38) and (currentplayer^.wounds > 4)) then begin
                  statusLine('  INNY GRACZ JEST ZA SILNY WYLECZ RANY');
                  isIn := false;
                  readkey;
                end;

                if ((room = 38) and (currentplayer^.wounds < 4)) then begin
                  i:= getPlayerIndexFromPosition(currentplayer^.x + 2 * dx, currentplayer^.y + 2 * dy);
                  opponentplayer := players[i];
                  statusLine('TOCZYSZ POJEDYNEK Z INNYM GRACZEM');
                  //writeln(i+1,' SILA',opponentplayer^.strength,currentplayer^.strength);
                  q := random(8);
                  if (currentplayer^.strength-opponentplayer^.strength+q>4) then BEGIN
                    write(': ZWYCIESTWO!');
                    if goldenkeyowner<6 then  goldenkeyowner:=currentplayerindex;
                    opponentplayer^.wounds := opponentplayer^.wounds+2;
                    //opponentplayer^.x := opponentplayer^.xstart;
                    //opponentplayer^.y := opponentplayer^.ystart;
                    //position(opponentplayer^.x, opponentplayer^.y);
                    //print(TILE_PLAYER);
                    isIn := true;
                  END else begin
                    write(': PRZEGRALES!');
                    if goldenkeyowner<6 then goldenkeyowner:=i;
                    currentplayer^.wounds := currentplayer^.wounds+2;
                    isIn := false;
                    //currentplayer^.x := currentplayer^.xstart;
                    //currentplayer^.y := currentplayer^.ystart;
                    //position(currentplayer^.x, currentplayer^.y);
                    //print(TILE_PLAYER);
                  END;
                  ShowStats;
                  readkey;
                end;

                if room = TILE_ROOM then begin
                    q := random(2);
                    if not waited and (currentplayer^.x > 8) and (currentplayer^.y > 3) and (random(10) >= 4) then begin
                        if q = 0 then room := TILE_DARK
                        else room := TILE_HOLE;
                    end else begin
                        r := random(6);
                        if r = 0 then begin
                            if q = 0 then room := TILE_DARK
                            else room := TILE_HOLE;
                        end;
                    end;
                end;

                if room = TILE_DARK then begin    //  ********************* dark room
                    position(currentplayer^.x + 2 * dx, currentplayer^.y + 2 * dy);
                    print(char(TILE_DARK));
                    statusLine('W POKOJU JEST CIEMNO,');
                    writeln;
                    if hasItem(itemSymbols[1]) then begin
                        write(texts[1], 'KAGANEK.');
                        isIn := true;
                    end else begin
                        write(texts[2], 'KAGANKA.');
                        isIn := false;
                    end;
                    readkey;
                    clearLine;
                end;
                if room = TILE_HOLE then begin    //  ********************* no floor
                    position(currentplayer^.x + 2 * dx, currentplayer^.y + 2 * dy);
                    print(char(TILE_HOLE));
                    statusLine('W POKOJU NIE MA POD'#12'OGI,');
                    writeln;
                    if hasItem(itemSymbols[3]) then begin
                        write(texts[1], 'DESK'#11'.');
                        isIn := true;
                    end else begin
                        write(texts[2], 'DESKI.');
                        isIn := false;
                    end;
                    readkey;
                end;
                if ((room = TILE_EXIT) and (goldenkeyowner<>currentplayerindex)) then begin
                  statusLine('POTRZEBUJESZ ZLOTEGO KLUCZA DO WYJSCIA');
                  isIn := false;
                  readkey;
                end;
                if ((room = TILE_EXIT) and (goldenkeyowner=currentplayerindex)) then begin  //  ********************* exit reached

                    position(currentplayer^.x, currentplayer^.y);
                    print(char(TILE_ROOM));
                    position(34, 16);
                    print(TILE_PLAYER);
                    statusLine('OSI'#2'GN'#2#12'E'#9' WYJ'#9'CIE, PODAJ HAS'#12'O.');
                    aStr:='';
                    position(2, 23);
                    for i := 1 to 4 do begin
                        keycode := getKey;
                        aStr[i] := char(keycode);
                        print(char(keycode));
                    end;
                    aStr[0] := chr(4);
                    if strCmp(aStr, currentPassword) then begin
                        statusLine('DZI'#11'KUJ'#11', TERAZ PROSZ'#11' ZAP'#12'ACI'#96);
                        ReadKey;
                        if currentplayer^.gold >= 100 then begin
                            currentplayer^.gold := currentplayer^.gold - 100;
                            ShowStats;
                            score := Trunc( currentplayer^.gold * currentplayer^.weapon);
                            statusLine('OPU'#9'CI'#12'E'#9' STARY DOM. WYNIOS'#12'E'#9' ');
                            writeln( currentplayer^.gold,'$');
                            write('I ',  currentplayer^.weaponName,' NA PAMI'#2'TK'#11'. WYNIK=',score);
                            Readkey;
                            gameEnded := true;
                        end else begin
                            statusLineln('OO! MASZ ZA MA'#12'O PIENI'#11'DZY.');
                            write('STRA'#21'NIK UCINA CI G'#12'OW'#11'.');
                            Readkey;
                            gameEnded := true;
                        end;
                    end else begin
                        statusLineln(#6'LE! HAS'#12'EM BY'#12'O ');
                        write(currentPassword, '.');
                        write('STRA'#21'NIK UCINA CI G'#12'OW'#11'.');
                        Readkey;
                        gameEnded := true;
                    end;

                end;

            end;

            if isIn and not gameEnded then begin // ***********   entered new room, update map
                position(currentplayer^.x, currentplayer^.y);
                print(char(currentplayer^.currentLocation));
                currentplayer^.x := currentplayer^.x + 2 * dx;
                currentplayer^.y := currentplayer^.y + 2 * dy;
                position(currentplayer^.x, currentplayer^.y);
                print(char(TILE_PLAYER));
                 currentplayer^.currentLocation := room;
                currentplayer^.energy := currentplayer^.energy - 0.5;
                stepFinished := true;
            end;


        end else begin  // **********************************   hit the wall
            statusLineln('MUR, UDERZY'#12'E'#9' SI'#11' W NOS.');
            write('T'#11'DY NIE PRZEJDZIESZ.');
            Readkey;
        end;
    end;

    if stepFinished and not gameEnded then begin  // ********************  random events

        r := random(16) + 1;
        itemLost := TILE_EMPTY_SLOT;
        case r of
            1: if hasItem(itemSymbols[0]) then begin
                    statusLine('M'#12'OTEK SI'#11' Z'#12'AMA'#12', ');
                    itemLost:=itemSymbols[0];
               end;
            2: if hasItem(itemSymbols[1]) then begin
                    statusLineln('W KAGANKU SKO'#14'CZY'#12'A SI'#11' OLIWA, ');
                    itemLost:=itemSymbols[1];
               end;
            3: if hasItem(itemSymbols[2]) then begin
                    statusLineln('KLUCZ JEST OD FORTEPIANU, ');
                    itemLost:=itemSymbols[2];
               end;
            4: if hasItem(itemSymbols[3]) then begin
                    statusLine('W DESCE S'#2' KORNIKI, ');
                    itemLost:=itemSymbols[3];
               end;
            5: if hasItem(itemSymbols[4]) then begin
                    statusLine('PROWIANT ZEPSU'#12' SI'#11', ');
                    itemLost:=itemSymbols[4];
               end;
            6: if hasItem(itemSymbols[5]) then begin
                    statusLine('NAP'#15'J JEST ST'#11'CH'#12'Y, ');
                    itemLost:=itemSymbols[5];
               end;
            7: if hasItem(itemSymbols[6]) then begin
                    statusLine('BANDA'#21' PODAR'#12' SI'#11', ');
                    itemLost:=itemSymbols[6];
               end;
            8: if hasItem(itemSymbols[7]) then begin
                    statusLine(#12'UBKI Z'#12'AMA'#12'Y SI'#11', ');
                    itemLost:=itemSymbols[7];
               end;
            9: if random(10) >= 5 then begin
                    foundPassword;
                    skipMonster := true;
               end;
            10,11:
                begin
                    statusLine('Z'#12'AMA'#12'E'#9' ');
                    write(  currentplayer^.weaponName, ', ');
                    currentplayer^.weapon := currentplayer^.weapon - 4;
                    if currentplayer^.weapon < 1 then currentplayer^.weapon := 1;
                     currentplayer^.weaponName := weapons[weapon - 1];
                    writeln('ZNALAZ'#12'E'#9' ',  currentplayer^.weaponName);
                    Readkey;
                    ShowStats;
                end;
            12: begin
                    q := random(2);
                    if q = 0 then begin
                        statusLine('ZOSTA'#12'E'#9' PRZENIESIONY NA START.');
                        position(currentplayer^.x, currentplayer^.y);
                        print(char(  currentplayer^.currentLocation));
                         currentplayer^.currentLocation := 32;
                         // todo set to player start
                        currentplayer^.x := 6;
                        currentplayer^.y := 1;
                        position(currentplayer^.x, currentplayer^.y);
                        print(TILE_PLAYER);
                        Readkey;
                        ShowStats;
                    end;
                end;
          13: if ((random(20) >= 18) and (goldenkeyowner=6)) then begin
            foundGoldenKey;
          end;

        end;

        if itemLost <> TILE_EMPTY_SLOT then begin
            write(texts[0]);
            delItem(itemLost);
            Readkey;
            ShowStats;
        end;

    end;

    if not skipMonster and not gameEnded then begin
        r := random(4);
        if r>0 then begin  // ********************************** encounter !!!
            q := random(30) + 1;
            monsterStrength := round((random * currentplayer^.strength * 2.65) + 1);
            monsterSize := monsterStrength;
            aStr := monsters[q - 1];
            bStr := ' CI'#11' ';
            if (q = 16) or (q = 23) or (q = 25) then bStr := 'A CI'#11' ';
            statusLine(texts[3]);
            writeln(bStr, aStr);
            write('SI'#12'A ', formatFloat(monsterStrength));

            stepFinished := false;
            repeat
                position(12,23);
                write('W'*'ALKA CZY ','O'*'KUP ?');
                keycode := getKey(87, 79);

                if keycode = 87 then begin  // ************** fight choosen

                    if ( currentplayer^.strength = 0) or ( currentplayer^.wounds > 4) then begin // ***********     too weak ?

                        statusLine('JESTE'#9' ZA S'#12'ABY, MUSISZ P'#12'ACI'#96);
                        payRansom;
                        readKey;

                    end else begin   // *****************************************************   fight

                        // get hurt
                        if ( currentplayer^.strength < monsterStrength * 1.2) and (random(10) >= 4) then
                            currentplayer^.wounds := currentplayer^.wounds + 1;
                        // hit monster
                        monsterStrength := round(monsterStrength - ((random * 2) + 1) * currentplayer^.strength * 0.57);

                        ShowStats;

                        if (monsterStrength <= 0) or (random(10) >= 5) then begin // ********* monster killed
                            r := random(5) + 1;  // loot size
                            statusLine(aStr);
                            write(' ZOSTA'#12);
                            if (q = 16) or (q = 23) or (q = 25) then writeln('A POKONANA, ')
                            else writeln(' POKONANY, ');
                            write('ZDOBY'#12'E'#9' ', treasures[r - 1]);
                            currentplayer^.gold := currentplayer^.gold + round(r * (1.25 * (1 + monsterSize / 15) + random));
                            stepFinished := true;
                            readKey;
                            ShowStats;
                        end else begin
                            statusLine(aStr);
                            write(' MA SI'#12#11' ', formatFloat(monsterStrength));
                        end;

                    end;
                end else payRansom; // ************** pay ranson
            until stepFinished;

        end;

        r := random(11) + 1; // ************************ loot
        case r of
            1,2,3,4,5,6,7,8: foundItem;
            9: foundWeapon;
            10: foundPassword;
        end;

    end;

    if not gameEnded then begin // *********************************** use items
        stepFinished := false;
        repeat
            statusLine('CHCESZ U'#21'YC JAKIEGO'#9' OBIEKTU (T/N) ?');
            keycode := getKey(84,78);
            if keycode=84 then begin
                statusLine2('KT'#15'REGO ?      ');
                repeat
                    keycode := getKey;
                until (keycode > 65) and (keycode < 90);
                if not hasItem(char(keycode)) then begin
                    statusLine2('NIE MASZ '#39);
                    write(chr(keycode), #39);
                    readkey;
                end else
                    if (keycode = 78) or (keycode = 80) or (keycode = 66) or (keycode = 76) then begin
                        delItem(char(keycode));
                        case keycode of
                            78: currentplayer^.energy := currentplayer^.energy + 1;
                            80: currentplayer^.energy := currentplayer^.energy + 3;
                            66: currentplayer^.wounds := currentplayer^.wounds - 1;
                            76: currentplayer^.wounds := currentplayer^.wounds - 3;
                        end;
                        if currentplayer^.wounds < 0 then currentplayer^.wounds := 0;
                        ShowStats;
                        if random(10)>=6 then stepFinished := true;
                    end else begin
                        statusLine2('TERAZ MO'#21'ESZ U'#21'Y'#96' TYLKO ');
                        write('N'*,' ','P'*,' ','B'*,' ','L'*);
                        ReadKey;
                    end;
            end else stepFinished := true;
        until stepFinished;
    end;

end;

procedure switchNextPlayer;
begin
  if currentplayerindex=numberofplayers-1 then BEGIN
    currentplayerindex := 0
  END else begin
    currentplayerindex := currentplayerindex+1;
  end
end;




begin
    numberofplayers := 3;
    currentplayerindex:= 0;
    randomize;
    assign(fscr, 'S:');
    rewrite(fscr, 1);
    SetIntVec(iDLI, @dli);

    repeat
        TitleScreen;
        ShowManual;
        PaintBoard;
        gameEnded := false;
        currentplayerindex:=0;
        switchCurrentPlayerVar;
        while not gameEnded do
        BEGIN
          MakeMove;
          switchNextPlayer;
          switchCurrentPlayerVar;
          statusLine('TURA GRACZA NR:');
          writeln(currentplayerindex+1);
          pause(200);
        END;

    until false;

    close(fscr);
end.
