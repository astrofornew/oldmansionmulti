
const
    monsters: array [0..MONSTERS_COUNT - 1] of TString = (
        'NIETOPERZ',
        'SZCZUR',
        'JASZCZUR',
        'PAJ'#2'K',
        'PIES',
        'W'#2#21,
        'KRONK',
        'SKRZAT',
        'GOBLIN',
        'ZOOMBIE',
        'WARG',
        'DUCH',
        'ELF',
        'WILKO'#12'AK',
        'CZAROWNICA',
        'ORK',
        'GARGOYLA',
        'TROLL',
        'CZAROWNIK',
        'WAMPIR',
        'UPI'#15'R',
        'MINOTAUR',
        'CYKLOP',
        'GIGANT',
        'HYDRA',
        'DEMON',
        'MEDUZA',
        'SMOK',
        'DIABE'#12,
        'FENIKS'
     );
    weapons: array [0..WEAPONS_COUNT - 1] of TString = (
        'WIDELEC','SCYZORYK','FINK'#11,'SZTYLET','SZPAD'#11,'LANC'#11,'OSZCZEP','SZABL'#11,'TOP'#15'R','MIECZ' );
    treasures: array [0..TREASURES_COUNT - 1] of TString = (
        'BR'#2'Z','SREBRO','Z'#12'OTO','PLATYN'#11,'DIAMENTY' );
    items: array [0..ITEMS_COUNT - 1] of TString = (
        'M'#12'OTEK','KAGANEK','KLUCZ','DESK'#11,'PROWIANT','NAPOJE','BANDA'#21,#12'UBKI','HAS'#12'O' );
    itemSymbols: array [0..ITEMS_COUNT - 2] of char = ( 'M', 'G', 'K', 'D', 'P', 'N', 'B', 'L');
    passwords: array [0..PASSWORDS_COUNT - 1] of TString = (
        'RYBA','JAJO','BAJT','TRZY','KLAN','KREW','MYSZ','KLOC','BICZ');
    texts: array [0..3] of TString = ('WI'#11'C WYRZUCI'#12'E'#9'.','U'#21'Y'#12'E'#9' ','NIE MASZ ','ZAATAKOWA'#12);



procedure TitleScreen;
var dl: word;
begin
    InitGraph(17);
    pause;
    nmien := $40;
    color2 := 0;
    dl := dpeek(560);
    poke(dl + 10,7);
    position(5, 5); print('STARY DOM');
    position(3, 9); print('gra przygodowa');
    position(3, 12); print('(1)one player');
    position(3, 13); print('(2)two players');
    position(3, 14); print('(3)three players');
    poke(dl + 25, 2);
    position(0, 18); print('PROGRAM:   POPRAWKI:');
    position(0, 20); print('W.Zientara - 1987     Bocianu - 2019');
    keycode := getKey;
    if keycode = 17 then numberofplayers:=1;
    if keycode = 18 then numberofplayers:=2;
    if keycode = 19 then numberofplayers:=3;
end;
